# Dump & restore savefile.

### This is a fork of vita-savmgr

## Development
```bash
mkdir build
cd build && cmake ..`
make
```
## Note
VitaShell modules are required.  
If VitaShell is not installed,  
copy kernel.skprx and user.suprx from VitaShell vpk to "ux0:data/vitaSaveManager/"  

## Controls
L/R - Switch tab (GAMES / SAVES)  
Triangle - Open menu  

## Features
1. Backup saves as decrypted or encrypted
2. Bulk backup of all your saves
3. Bulk delete of all your backups
4. Restore encrypted and decrypted saves
5. Change savefile region
6. More..

## License
GPLv3

## Credits
Project use these project's codes.

* [VitaShell][]
* [rinCheat][]
* [vita-savemgr][]

[modules]: https://github.com/TheOfficialFloW/VitaShell/tree/master/modules
[VitaShell]: https://github.com/TheOfficialFloW/VitaShell
[rinCheat]: https://github.com/Rinnegatamante/rinCheat
[vita-savemgr]:
https://github.com/d3m3vilurr/vita-savemgr
