#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include "file.h"

char *blacklists[] = {
    "sce_pfs/",
    "sce_sys/safemem.dat",
    "sce_sys/keystone",
    //"sce_sys/param.sfo",
    "sce_sys/sealedkey",
    NULL,
};

int exists(const char *path) {
	SceIoStat stat = {0};

	return sceIoGetstat(path, &stat) >= 0;
}

int is_dir(const char *path) {
	SceIoStat stat = {0};

	if (sceIoGetstat(path, &stat) < 0)
		return 0;

	return SCE_S_ISDIR(stat.st_mode);
}

int mkdir(const char *path, int mode) {
	if (is_dir(path))
		return 1;

	int len = strlen(path);
	char npath[len];
	int start = 0;
	int ret;

	memset(npath, 0, len);

	for (int i=0; i<len; ++i) {
		npath[i] = path[i];

		if (!start < 2 && path[i] == ':')
			start = 1;

		if (path[i] != '/' || !start || is_dir(npath))
			continue;

		ret = sceIoMkdir(npath, mode);
		if (ret < 0)
			return 0;
	}

	ret = sceIoMkdir(path, mode);
	if (ret < 0)
		return 0;

	return 1;
}

int rmdir(const char *path, void (*callback)(int*, int), int *curr, int max) {
    SceUID dfd = sceIoDopen(path);

    if (dfd < 0) {
        int ret = sceIoRemove(path);

        if (ret < 0)
            return ret;
    }

    int res = 0;
    do {
        SceIoDirent dir;
        memset(&dir, 0, sizeof(SceIoDirent));

        res = sceIoDread(dfd, &dir);

        if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
            continue;

		size_t len = sizeof(char) * (strlen(path) + strlen(dir.d_name) + 2);
        char *new_path = malloc(len);

        snprintf(new_path, len, "%s/%s", path, dir.d_name);

        if (SCE_S_ISDIR(dir.d_stat.st_mode)) {
            int ret = rmdir(new_path, callback, curr, max);

            if (ret <= 0) {
                free(new_path);
                sceIoDclose(dfd);
                return ret;
            }

        } else {
            int ret = sceIoRemove(new_path);
            if (callback)
                callback(curr, max);

            if (ret < 0) {
                free(new_path);
                sceIoDclose(dfd);
                return ret;
            }
        }

        free(new_path);
    } while (res > 0);

    sceIoDclose(dfd);

    int ret = sceIoRmdir(path);
    if (ret < 0)
        return ret;

    return 1;
}

int copyfile(const char *src, const char *dest, int check_blacklist) {
    if (strcasecmp(src, dest) == 0)
        return 1;
    else if (!exists(src))
        return 0;

    int i = 0;
    while (check_blacklist && blacklists[i]) {
        if (strstr(dest, blacklists[i]))
            return 2;

        ++i;
    }

    int ignore_error = strncmp(dest, "savedata0:", 10) == 0;
	void *buf = memalign(4096, 128 * 1024);
	SceUID fsrc = sceIoOpen(src, SCE_O_RDONLY, 0);
	SceUID fdst = sceIoOpen(dest, SCE_O_WRONLY | SCE_O_CREAT, 0777);
	int read = 0, written = 0;
	SceIoStat stat;

	if (!ignore_error && (fsrc < 0 || fdst < 0)) {
		free(buf);
		sceIoClose(fsrc);
		sceIoClose(fdst);
		return 0;
	}

	while (1) {
		read = sceIoRead(fsrc, buf, 128 * 1024);
		if (read < 0) {
			free(buf);
			sceIoClose(fsrc);
			sceIoClose(fdst);
			sceIoRemove(dest);
			return 0;

		} else if (!read) {
			break;
		}

		written = sceIoWrite(fdst, buf, read);
		if (written < 0) {
			free(buf);
			sceIoClose(fsrc);
			sceIoClose(fdst);
			sceIoRemove(dest);
			return 0;
		}
	}

	free(buf);

	// Inherit file stat
  	memset(&stat, 0, sizeof(SceIoStat));
  	sceIoGetstatByFd(fsrc, &stat);
	sceIoChstatByFd(fdst, &stat, 0x3B);

	sceIoClose(fsrc);
	sceIoClose(fdst);

	return 1;
}

int copydir(const char *src, const char *dest, void (*callback)(int*, int), int *curr, int max, int check_blacklist) {
    if (strcasecmp(src, dest) == 0)
        return 1;

    int i = 0;
    while (check_blacklist && blacklists[i]) {
        if (strstr(dest, blacklists[i]))
            return 2;
        ++i;
    }

	SceUID dfd = sceIoDopen(src);
	SceIoStat stat;
	int res = 0, ret = 0;

	if (dfd < 0) // not a folder?
		return copyfile(src, dest, check_blacklist);

    memset(&stat, 0, sizeof(SceIoStat));
    sceIoGetstatByFd(dfd, &stat);

    stat.st_mode |= SCE_S_IWUSR;

    if (!exists(dest)) {
        ret = sceIoMkdir(dest, stat.st_mode & 0xFFF);
        if (ret < 0 && ret != SCE_ERROR_ERRNO_EEXIST) {
            sceIoDclose(dfd);
            return 0;
        }
    }

    if (ret == SCE_ERROR_ERRNO_EEXIST)
		sceIoChstat(dest, &stat, 0x3B);

	do {
		SceIoDirent dir;

		memset(&dir, 0, sizeof(SceIoDirent));

		res = sceIoDread(dfd, &dir);
		if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
			continue;

		size_t dPathLen = strlen(dir.d_name);
		size_t srcLen = strlen(src) + dPathLen + 4;
		size_t dstLen = strlen(dest) + dPathLen + 4;
		char *new_src = malloc(srcLen);
		char *new_dst = malloc(dstLen);

		snprintf(new_src, sizeof(char) * srcLen, "%s/%s", src, dir.d_name);
		snprintf(new_dst, sizeof(char) * dstLen, "%s/%s", dest, dir.d_name);

		if (SCE_S_ISDIR(dir.d_stat.st_mode)) {
			ret = copydir(new_src, new_dst, callback, curr, max, check_blacklist);

		} else {
			ret = copyfile(new_src, new_dst, check_blacklist);
            if (callback)
                callback(curr, max);
        }

		free(new_src);
		free(new_dst);

		if (!ret) {
			sceIoDclose(dfd);
			return 0;
		}
	} while (res > 0);

	sceIoDclose(dfd);

	return 1;
}

int file_count(const char *path, int check_blacklist) {
    if (!exists(path))
        return 0; // TODO? :/

	int i = 0;
	while (check_blacklist && blacklists[i]) {
		if (strstr(path, blacklists[i]))
			return 0;

		++i;
	}

    SceUID dfd = sceIoDopen(path);
    int cnt = 0, res = 0;

    do {
        SceIoDirent dir;
        memset(&dir, 0, sizeof(SceIoDirent));

        res = sceIoDread(dfd, &dir);

        if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0)
            continue;

		size_t len = sizeof(char) * (strlen(path) + strlen(dir.d_name) + 2);
        char *new_path = malloc(len);

        snprintf(new_path, len, "%s/%s", path, dir.d_name);

        if (SCE_S_ISDIR(dir.d_stat.st_mode))
            cnt += file_count(new_path, check_blacklist);
        else
            ++cnt;

        free(new_path);
    } while (res > 0);

    sceIoDclose(dfd);
    return cnt;
}

int folder_count(const char *path) {
	if (!exists(path))
        return -1;

	SceUID dfd = sceIoDopen(path);
    int cnt = 0, res = 0;

	if (dfd < 0)
		return 0;

	do {
        SceIoDirent dir;
        memset(&dir, 0, sizeof(SceIoDirent));

        res = sceIoDread(dfd, &dir);

        if (res <= 0 || strcmp(dir.d_name, ".") == 0 ||
				strcmp(dir.d_name, "..") == 0 || !SCE_S_ISDIR(dir.d_stat.st_mode))
            continue;

        ++cnt;
    } while (res > 0);

	sceIoDclose(dfd);
	return cnt;
}