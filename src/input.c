#include <psp2/shellutil.h>
#include <psp2/apputil.h>
#include <psp2/system_param.h>

#include "common.h"
#include "input.h"

void init_input() {
    int enter_button;

    sceCtrlSetSamplingMode(SCE_CTRL_MODE_ANALOG_WIDE);

    SceAppUtilInitParam init_param = {0};
    SceAppUtilBootParam boot_param = {0};
    sceAppUtilInit(&init_param, &boot_param);

    sceAppUtilSystemParamGetInt(SCE_SYSTEM_PARAM_ID_ENTER_BUTTON, &enter_button);

    if (enter_button == SCE_SYSTEM_PARAM_ENTER_BUTTON_CIRCLE) {
        SCE_CTRL_ENTER = SCE_CTRL_CIRCLE;
        SCE_CTRL_CANCEL = SCE_CTRL_CROSS;

        strcpy(ICON_ENTER, ICON_CIRCLE);
        strcpy(ICON_CANCEL, ICON_CROSS);

    } else {
        SCE_CTRL_ENTER = SCE_CTRL_CROSS;
        SCE_CTRL_CANCEL = SCE_CTRL_CIRCLE;

        strcpy(ICON_ENTER, ICON_CROSS);
        strcpy(ICON_CANCEL, ICON_CIRCLE);
    }

    return;
}

void lock_psbutton() {
    sceShellUtilLock(SCE_SHELL_UTIL_LOCK_TYPE_PS_BTN |
                     SCE_SHELL_UTIL_LOCK_TYPE_QUICK_MENU);

    return;
}

void unlock_psbutton() {
    sceShellUtilUnlock(SCE_SHELL_UTIL_LOCK_TYPE_PS_BTN |
                       SCE_SHELL_UTIL_LOCK_TYPE_QUICK_MENU);

    return;
}

int read_buttons() {
    SceCtrlData pad = {0};
    static int old;
    static int hold_times;
    int curr, btn;

    sceCtrlPeekBufferPositive(0, &pad, 1);

    if (pad.ly < 0x10)
        pad.buttons |= SCE_CTRL_UP;
    else if (pad.ly > 0xef)
        pad.buttons |= SCE_CTRL_DOWN;
    else if (pad.lx < 0x10)
        pad.buttons |= SCE_CTRL_LEFT;
    else if (pad.lx > 0xef)
        pad.buttons |= SCE_CTRL_RIGHT;

    curr = pad.buttons;
    btn = pad.buttons & ~old;

    if (curr && old == curr) {
        ++hold_times;

        if (hold_times >= 10) {
            btn = curr;
            hold_times = 8;
            btn |= SCE_CTRL_HOLD;
        }

    } else {
        hold_times = 0;
        old = curr;
    }

    return btn;
}
