#include <stdint.h>

#include "sqlite3.h"
#include "utils.h"

void setRegionLabel(char regionID, appinfo *info) {
    switch (regionID) {
        case 'B':
        case 'F':
            strncpy(info->region, "EU", 2);
            break;
        case 'C':
        case 'G':
            strncpy(info->region, "JP", 2);
            break;
        case 'D':
        case 'H':
            strncpy(info->region, "ASIA", 4);
            break;
        case 'A':
        case 'E':
            strncpy(info->region, "US", 2);
            break;
        case 'I':
            strncpy(info->region, "INT", 3);
            break;
        default:
            strncpy(info->region, "N/A", 3);
            break;
    }

    return;
}

static void get_orphan_saves(applist *list) {
    SceUID dfd = sceIoDopen("ux0:user/00/savedata");
    int res = 0;

    if (dfd < 0)
        return;

    do {
        SceIoDirent dir;
        memset(&dir, 0, sizeof(SceIoDirent));

        res = sceIoDread(dfd, &dir);

        if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0 ||
                dir.d_name[0] != 'P' || !SCE_S_ISDIR(dir.d_stat.st_mode))
            continue;

        if (dir.d_name[1] == 'C' && dir.d_name[2] == 'S') {
            appinfo *tmp = list->items;

            while (tmp) {
                if (strcmp(tmp->title_id, dir.d_name) == 0)
                    break;

                tmp = tmp->next;
            }

            if (!tmp) {
                appinfo *info = calloc(1, sizeof(appinfo));

                info->is_installed = 0;
                strncpy(info->title_id, dir.d_name, sizeof(info->title_id));
                strncpy(info->real_id, dir.d_name, sizeof(info->real_id));
                setRegionLabel(dir.d_name[3], info);

                list->tail->next = info;
                info->prev = list->tail;
                list->tail = info;

                ++list->count;
            }
        }
    } while (res > 0);

	sceIoDclose(dfd);

    return;
}

static int get_applist_callback(void *data, int argc, char **argv, char **cols) {
    applist *list = (applist*)data;
    appinfo *info = calloc(1, sizeof(appinfo));

    if (list->count == 0) {
        list->items = list->tail = info;

    } else {
        list->tail->next = info;
        info->prev = list->tail;
        list->tail = info;
    }

    ++list->count;

    info->is_installed = 1;
    strcpy(info->title_id, argv[0]);
    strcpy(info->real_id, argv[1]);
    strcpy(info->title, argv[2]);
    strcpy(info->eboot, argv[3]);
    strcpy(info->dev, argv[4]);
    strcpy(info->iconpath, argv[5]);

    for (int i=0; i<256; ++i) {
        if (info->title[i] == '\n')
            info->title[i] = ' ';
    }

    setRegionLabel(info->title_id[3], info);

    return 0;
}

int get_applist(applist *list) {
    const char *query = "select a.titleid, b.realid, c.title, d.ebootbin,"
                  "       rtrim(substr(d.ebootbin, 0, 5), ':') as dev,"
                  "       e.iconpath"
                  "  from (select titleid"
                  "          from tbl_appinfo"
                  "         where key = 566916785"
                  "           and titleid like 'PCS%'"
                  "         order by titleid) a,"
                  "       (select titleid, val as realid"
                  "          from tbl_appinfo"
                  "         where key = 278217076) b,"
                  "       tbl_appinfo_icon c,"
                  "       (select titleid, val as ebootbin"
                  "          from tbl_appinfo"
                  "         where key = 3022202214) d,"
                  "       (select titleid, iconpath"
                  "          from tbl_appinfo_icon"
                  "         where type = 0) e"
                  " where a.titleid = b.titleid and a.titleid = c.titleid"
                  "   and a.titleid = d.titleid and a.titleid = e.titleid";
    sqlite3 *db;
    char *errMsg;

    int ret = sqlite3_open(APP_DB, &db);
    if (ret)
        return -1;


    ret = sqlite3_exec(db, query, get_applist_callback, (void *)list, &errMsg);
    if (ret != SQLITE_OK) {
        sqlite3_close(db);
        return -2;
    }

    sqlite3_close(db);

    get_orphan_saves(list);

    if (list->count < 1)
        return -3;

    return 0;
}

int get_savelist(applist *savelist) {
    SceUID dfd = sceIoDopen(savemgr_fpath);
    int res;

    if (dfd < 0)
        return -1;

    do {
        SceIoDirent dir;
        appinfo *saveinfo;
        char iconPath[54] = {0};

        memset(&dir, 0, sizeof(SceIoDirent));
        saveinfo = calloc(1, sizeof(appinfo));

        res = sceIoDread(dfd, &dir);

        if (res <= 0 || strcmp(dir.d_name, ".") == 0 || strcmp(dir.d_name, "..") == 0 || !SCE_S_ISDIR(dir.d_stat.st_mode))
            continue;

        if (savelist->count == 0) {
            savelist->items = savelist->tail = saveinfo;

        } else {
            savelist->tail->next = saveinfo;
            saveinfo->prev = savelist->tail;
            savelist->tail = saveinfo;
        }

        ++savelist->count;

        strcpy(saveinfo->title_id, dir.d_name);
        strcpy(saveinfo->real_id, dir.d_name);

        snprintf(iconPath, sizeof(iconPath), "%s/%s/icon.png", savemgr_fpath, dir.d_name);
        strcpy(saveinfo->iconpath, iconPath);

        setRegionLabel(saveinfo->title_id[3], saveinfo);
    } while (res > 0);

    sceIoDclose(dfd);

    return 0;
}

int update_list(applist *list, int cmd, const char *titleID) {
    int ret = 0;

    switch (cmd) {
        case 0: { // Add new item
            char iconPath[54] = {0};
            appinfo *tmp = list->items;
            appinfo *info;

            while (tmp && strcmp(tmp->title_id, titleID) != 0)
                tmp = tmp->next;

            if (tmp != NULL)
                break;

            info = calloc(1, sizeof(appinfo));

            if (list->count == 0) {
                list->items = list->tail = info;

            } else {
                list->tail->next = info;
                info->prev = list->tail;
                list->tail = info;
            }

            ++list->count;

            list->curr = list->choose = list->items;

            strcpy(info->title_id, titleID);
            strcpy(info->real_id, titleID);

            snprintf(iconPath, sizeof(iconPath), "%s/%s/icon.png", savemgr_fpath, titleID);
            strcpy(info->iconpath, iconPath);

            setRegionLabel(info->title_id[3], info);
        }
            break;
        case 1: { // Delete item
            appinfo *tmp = list->items;

            while (tmp && strcmp(tmp->title_id, titleID) != 0)
                tmp = tmp->next;

            if (tmp == NULL)
                break;

            if (tmp->prev) {
                tmp->prev->next = tmp->next;

            } else {
                list->items = tmp->next;

                if (!list->items || !list->items->next)
                    list->tail = list->items;
            }

            if (tmp->next)
                tmp->next->prev = tmp->prev;
            else
                list->tail = tmp->prev;

            --list->count;

            list->curr = list->choose = list->items;

            unload_icon(tmp);
            free(tmp);
        }
            break;
        default:
            ret = -1;
            break;
    }

    return ret;
}

void free_list(applist *list) {
    appinfo *tmp = list->items;

    while (tmp) {
        appinfo *aux = tmp->next;

        unload_icon(tmp);
        free(tmp);

        tmp = aux;
    }

    list->count = 0;
    list->curr = list->choose = list->tail = list->items = NULL;

    return;
}

void load_icon(appinfo *info) {
    if (info->icon.texture)
        return;

    if (!info->icon.buf && exists(info->iconpath)) {
        info->icon.buf = calloc(sizeof(uint8_t), ICON_BUF_SIZE);
        FILE *f = fopen(info->iconpath, "r");

        fread(info->icon.buf, sizeof(uint8_t), ICON_BUF_SIZE, f);
        fclose(f);
    }

    if (info->icon.buf)
        info->icon.texture = vita2d_load_PNG_buffer(info->icon.buf);

    if (!info->icon.texture) {
        extern unsigned char _binary_img_noicon_png_start;

        unload_icon(info);
        info->icon.texture = vita2d_load_PNG_buffer(&_binary_img_noicon_png_start);
    }

    return;
}

void unload_icon(appinfo *info) {
    if (info->icon.buf) {
        free(info->icon.buf);
        info->icon.buf = NULL;
    }

    if (info->icon.texture) {
        vita2d_free_texture(info->icon.texture);
        info->icon.texture = NULL;
    }

    return;
}
