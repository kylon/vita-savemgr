#include "common.h"
#include "display.h"
#include "input.h"

static int countLines(const char *str) {
    size_t len = sizeof(char) * strlen(str) + 1;
    char *tmp = malloc(len);
    int c = 1;

    if (!tmp)
        return c;

    snprintf(tmp, len, "%s", str);
    tmp = strchr(tmp, '\n');

    while (tmp) {
        snprintf(tmp, len, "%s", ++tmp);
        tmp = strchr(tmp, '\n');
        ++c;
    }

    free(tmp);

    return c;
}

void init_console() {
    size_t enterLen = strlen(ICON_ENTER);
    size_t len = sizeof(char) * (strlen(ICON_CANCEL) + enterLen + 20);

    confirm_msg = malloc(len); //TODO check
    snprintf(confirm_msg, len, "%s CANCEL    %s CONFIRM", ICON_CANCEL, ICON_ENTER);
    confirm_msg_width = vita2d_pgf_text_width(font, 1.0, confirm_msg);

    len = sizeof(char) * (len - 8);
    yesno_msg = malloc(len);
    snprintf(yesno_msg, len, "%s No    %s Yes", ICON_CANCEL, ICON_ENTER);
    yesno_msg_width = vita2d_pgf_text_width(font, 1.0, yesno_msg);

    len = sizeof(char) * (enterLen + 8);
    close_msg = malloc(len);
    snprintf(close_msg, len, "%s CLOSE", ICON_ENTER);
    close_msg_width = vita2d_pgf_text_width(font, 1.0, close_msg);

    return;
}

int confirm(const char *msg, float zoom, int yesno) {
    int text_width = vita2d_pgf_text_width(font, zoom, msg);
    int text_height = vita2d_pgf_text_height(font, zoom, msg);

    int padding = 50;
    int width = text_width + (padding * 2);
    int height = text_height + (padding * 2);
    int left = (SCREEN_WIDTH - width) / 2;
    int top = (SCREEN_HEIGHT - height) / 2;
    int textXW = (width - (!yesno ? confirm_msg_width:yesno_msg_width)) / 2;

    vita2d_start_drawing();

    vita2d_draw_rectangle(left-5, top-5, width+10, height+10, LIGHT_GRAY);
    vita2d_draw_rectangle(left, top, width, height, LIGHT_SLATE_GRAY);

    vita2d_pgf_draw_text(font, left + padding, top + padding, WHITE, zoom, msg);
    vita2d_pgf_draw_text(font, left + textXW,
                         top + height - 25, WHITE, zoom,
                         (!yesno ? confirm_msg:yesno_msg));
    vita2d_end_drawing();
    vita2d_wait_rendering_done();
    vita2d_swap_buffers();

    while (1) {
        int btn = read_buttons();

        if (btn & SCE_CTRL_HOLD)
            continue;
        else if (btn & SCE_CTRL_ENTER)
            return CONFIRM;
        else if (btn & SCE_CTRL_CANCEL)
            return CANCEL;
    }
}

int alert(const char *msg, float zoom) {
    int text_width = vita2d_pgf_text_width(font, zoom, msg);
    int text_height = vita2d_pgf_text_height(font, zoom, msg);

    int padding = 50;
    int width = text_width + (padding * 2);
    int height = text_height + (padding * 2);

    int left = (SCREEN_WIDTH - width) / 2;
    int top = (SCREEN_HEIGHT - height) / 2;

    vita2d_start_drawing();

    vita2d_draw_rectangle(left-5, top-5, width+10, height+10, LIGHT_GRAY);
    vita2d_draw_rectangle(left, top, width, height, LIGHT_SLATE_GRAY);

    vita2d_pgf_draw_text(font, left + padding, top + padding, WHITE, zoom, msg);
    vita2d_pgf_draw_text(font,
                         left + ((width - close_msg_width) / 2),
                         top + height - 25, WHITE, zoom, close_msg);

    vita2d_end_drawing();
    vita2d_wait_rendering_done();
    vita2d_swap_buffers();

    while (1) {
        int btn = read_buttons();

        if (btn & SCE_CTRL_HOLD)
            continue;
        else if (btn & SCE_CTRL_ENTER)
            return 0;
    }
}

void draw_progress(int current, int max) {
    vita2d_start_drawing();

    int width = 500;
    int height = 140;
    int left = (SCREEN_WIDTH - width) / 2;
    int top = (SCREEN_HEIGHT - height) / 2;

    int gauge_width = 400;
    int gauge_height = 40;
    int gauge_left = left + 50;
    int gauge_top = top + 50;

    int progress_width = current * gauge_width / max;

    vita2d_draw_rectangle(left, top, width, height, GRAY);
    vita2d_draw_rectangle(gauge_left, gauge_top, gauge_width, gauge_height, BLACK);

    vita2d_draw_rectangle(gauge_left, gauge_top, progress_width, gauge_height, LIGHT_SKY_BLUE);

    vita2d_end_drawing();
    vita2d_wait_rendering_done();
    vita2d_swap_buffers();

    return;
}

void init_progress(int max) {
    draw_progress(0, max);
    return;
}

void incr_progress(int *current, int max) {
    ++(*current);
    draw_progress(*current, max);
    return;
}
