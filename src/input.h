#pragma once

#include <psp2/ctrl.h>

void init_input();
void lock_psbutton();
void unlock_psbutton();
int read_buttons();
