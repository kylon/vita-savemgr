#pragma once

#include <psp2/io/stat.h>
#include <psp2/io/fcntl.h>
#include <psp2/io/dirent.h>

#define SCE_ERROR_ERRNO_EEXIST (int)(0x80010011)

int exists(const char *path);
int is_dir(const char *path);
int mkdir(const char *path, int mode);
int rmdir(const char *path, void (*callback)(), int *curr, int max);
int copyfile(const char *src, const char *dest, int check_blacklist);
int copydir(const char *src, const char *dest, void (*callback)(), int *curr, int max, int check_blacklist);
int file_count(const char *path, int check_blacklist);
int folder_count(const char *path);
